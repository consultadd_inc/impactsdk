import requests
import xmltodict
import sys
from impactsdk.constants import OFFLINE_METHODS, ONLINE_METHODS, SOAP_BODY_OFFLINE, SOAP_BODY_ONLINE, \
    IMPACT_OFFLINE_URL, IMPACT_ONLINE_URL


def impact_call(impact_call, method_name, *args):

    if impact_call == "offline":
        soap_body = SOAP_BODY_OFFLINE
        sub_template = OFFLINE_METHODS[method_name]
        impact_url = IMPACT_OFFLINE_URL

    elif impact_call == "online":
        soap_body = SOAP_BODY_ONLINE
        sub_template = ONLINE_METHODS[method_name]
        impact_url = IMPACT_ONLINE_URL

    sub_template = sub_template.format(*args)
    soap_body = soap_body.format(method_name, sub_template)
    headers = {'content-type': 'application/soap+xml', 'charset': 'utf-8'}

    rr = requests.post(impact_url, data=soap_body, headers=headers)

    if rr.status_code == 200:
        response = xmltodict.parse(rr.text)
        response = response['s:Envelope']['s:Body']
        return response
    else:
        return {"failure": rr.text, "status code": rr.status_code}