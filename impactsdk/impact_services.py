from impactsdk.soap_client import impact_call
import xmltodict
import json


class ImpactService():
    def __init__(self):
        super().__init__()

    def get_seatlayout_offline(self, *data):
        raw_data = impact_call("offline", "GetBlankScreenLayout", *data)
        seat_layout = raw_data['GetBlankScreenLayoutResponse']['GetBlankScreenLayoutResult']['b:BlankSeatLayout']
        return json.dumps(seat_layout)

    def get_seatlayout_online(self, *data):
        raw_data = impact_call("online", "GetBlockedSeatsForLayout", *data)
        blocked_seat_list = raw_data['GetBlockedSeatsForLayoutResponse']['GetBlockedSeatsForLayoutResult']['b:BlockedSeats']
        return json.dumps(blocked_seat_list)

    def lock_seats(self, *data):
        raw_data = impact_call("online", "GetBlockedTickets", *data)
        blocktickets = raw_data['GetBlockedTicketsResponse']['GetBlockedTicketsResult']
        return json.dumps(blocktickets)

    def book_seats(self, *data):
        raw_data = impact_call("online", "GetBookingDetails", *data)
        booking_result = raw_data['GetBookingDetailsResponse']['GetBookingDetailsResult']
        return json.dumps(booking_result)

    def cancel_transaction(self, *data):
        raw_data = impact_call("online", "ReleaseBlockedTickets", *data)
        booking_result = raw_data['ReleaseBlockedTicketsResponse']['ReleaseBlockedTicketsResult']
        return json.dumps(booking_result)

    # def cancel_booking(self, *data):
    # return impact_call("","",*data) #maybe refund transaction

    def get_movie(self, *data):
        raw_data = impact_call("offline", "GetMovieDetails", *data)
        movie_details = raw_data['GetMovieDetailsResponse']['GetMovieDetailsResult']['b:Movie']
        return json.dumps(movie_details)  #get single movie

    def get_theatre(self, *data):
        raw_data = impact_call("offline", "GetTheaterDetails", *data)  #getsingletheatre
        theatre_details = raw_data['GetTheaterDetailsResponse']['GetTheaterDetailsResult']['b:Theater']
        return json.dumps(theatre_details)

    def get_theatreclass(self, *data):
        raw_data = impact_call("offline", "GetTheaterClassDetails", *data)
        classes = raw_data['GetTheaterClassDetailsResponse']['GetTheaterClassDetailsResult']['b:TheaterClass']
        return json.dumps(classes)

    def get_theatrescreen(self, *data):
        raw_data = impact_call("offline", "GetTheaterScreenDetails", *data)
        screens = raw_data['GetTheaterScreenDetailsResponse']['GetTheaterScreenDetailsResult']['b:TheaterScreen']
        return json.dumps(screens)

    def get_theatrescreenclass(self, *data):
        raw_data = impact_call("offline", "GetTheaterScreenClassDetails", *data)
        classes = raw_data['GetTheaterScreenClassDetailsResponse']['GetTheaterScreenClassDetailsResult']['b:TheaterScreenClass']
        return json.dumps(classes)

    def get_theatreshows(self, *data):
        raw_data = impact_call("offline", "GetExhibitorTheaterShowDetails", *data)
        theatre_shows = raw_data['GetExhibitorTheaterShowDetailsResponse']['GetExhibitorTheaterShowDetailsResult']['b:TheaterShow']
        return json.dumps(theatre_shows)

    def get_showprice(self, *data):
        raw_data = impact_call("offline", "GetExhibitorShowRatesDetails", *data)
        show_prices = raw_data['GetExhibitorShowRatesDetailsResponse']['GetExhibitorShowRatesDetailsResult']['b:ShowRates']
        return json.dumps(show_prices)

    def authentication(self, *data):
        raw_data = impact_call("online", "Authentication", *data)
        authentication_data = raw_data['AuthenticationResponse']['AuthenticationResult']
        return json.dumps(authentication_data)

    def check_session(self, *data):
        raw_data = impact_call("online", "CheckSession", *data)
        check_session_data = raw_data['CheckSessionResponse']['CheckSessionResult']
        return json.dumps(check_session_data)

    def get_referenceID(self, *data):
        raw_data = impact_call("online", "GetReferenceID", *data)
        check_reference_data = raw_data['GetReferenceIDResponse']['GetReferenceIDResult']
        return json.dumps(check_reference_data)

    def get_refund(self, *data):
        raw_data = impact_call("online", "MarkRefundTransaction", *data)
        refundtransaction = raw_data['MarkRefundTransactionResponse']['MarkRefundTransactionResult']
        return json.dumps(refundtransaction)